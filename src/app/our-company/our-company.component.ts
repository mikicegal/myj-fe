import { Component, OnInit } from '@angular/core';
import { UsService } from '../admin-services/us.service';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-our-company',
  templateUrl: './our-company.component.html',
  styleUrls: ['./our-company.component.scss']
})
export class OurCompanyComponent implements OnInit {
  missionConfig;
  historyConfig;
  ourCompanyData;
  valuesArray;
  missionCarousel = [];
  historyImage;

  constructor(private usService: UsService, public loadingService: LoadingService) {

  }
  isJSON(json) {
    try {
      return JSON.parse(json);
    } catch (e) {
      return json;
    }
  }
  ngOnInit() {
    this.loadingService.showLoading();
    this.usService.gerOurCompanyPage().subscribe((response: any) => {
      this.ourCompanyData = response.data.how_us;
      this.ourCompanyData.mision = this.isJSON(this.ourCompanyData.mision);
      this.ourCompanyData.vision = this.isJSON(this.ourCompanyData.vision);
      this.ourCompanyData.historia = this.isJSON(this.ourCompanyData.historia);
      response.data.images_mision.forEach(element => {
        this.missionCarousel.push({ image: element.url_image });
      });
      this.historyImage = response.data.images_history[0].url_image;
      /* .forEach(element => {
        this.historyImage.push({image: element.url_image});
      }); */
      this.missionConfig = {
        arraySliders: this.missionCarousel,
        numberSliderPerView: 1,
        transitionSpeed: 8000,
        thumbnails: true
      };
      /*  this.historyConfig = {
         arraySliders: this.historyImage,
         numberSliderPerView: 1,
         transitionSpeed: 8000,
         thumbnails: true
       }; */
      this.valuesArray = response.data.valors;
      this.loadingService.hideLoading();
    });

  }

}
