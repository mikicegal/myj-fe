import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../admin-services/service.service';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  constructor(private serviceService: ServiceService, public loadingService: LoadingService) { }
  servicesArray;
  banner;
  ngOnInit() {
    this.loadingService.showLoading();
    this.serviceService.getListServices().subscribe((response: any) => {
      this.servicesArray = response.data.services;
      this.banner = response.data.banner;
      this.loadingService.hideLoading();
    });
  }

}
