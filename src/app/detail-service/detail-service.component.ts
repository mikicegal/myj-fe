import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServiceService } from '../admin-services/service.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-detail-service',
  templateUrl: './detail-service.component.html',
  styleUrls: ['./detail-service.component.scss']
})
export class DetailServiceComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private serviceService: ServiceService,
    private router: Router,
    public loadingService: LoadingService) { }
  detailServiceData;
  images;
  urlImages;
  isSpecial;
  routerSubscription: Subscription;
  mainConfig: any;
  showCarousel = false;
  isJSON(json) {
    try {
      return JSON.parse(json);
    } catch (e) {
      return json;
    }
  }

  ngOnInit() {
    this.loadingService.showLoading();
    this.routerSubscription = this.router.events.subscribe((change) => {
      if (change instanceof NavigationEnd) {
        this.retrieveData();
      }
    });
    this.retrieveData();

  }
  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }

  private retrieveData() {
    this.serviceService.getDetailService(this.route.snapshot.params.id).subscribe((response: any) => {
      this.detailServiceData = response.data.services;
      this.isSpecial = response.data.have_bim;
      this.detailServiceData.descripcion = this.isJSON(response.data.services.descripcion);
      this.images = response.data.images;
      this.urlImages = new Array();
      this.images.forEach(element => {
        this.urlImages.push({ image: element.url_image });
      });
      this.mainConfig = {
        arraySliders: this.urlImages,
        numberSliderPerView: 1,
        transitionSpeed: 8000
      };
      this.loadingService.hideLoading();
    });
  }
  toggleCarousel() {
    this.showCarousel = !this.showCarousel;
  }
}
