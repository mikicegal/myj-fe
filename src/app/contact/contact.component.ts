import { Component, OnInit } from '@angular/core';
import { ContactService } from '../admin-services/contact.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor(private contactService: ContactService, private sanitizer: DomSanitizer, public loadingService: LoadingService) { }
  contactData: any;
  sendMailForm: FormGroup;

  isJSON(json) {
    try {
      return JSON.parse(json);
    } catch (e) {
      return json;
    }
  }

  ngOnInit() {
    this.loadingService.showLoading();
    this.sendMailForm = new FormGroup({
      nombre: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      asunto: new FormControl('', Validators.required),
      mensaje: new FormControl('', Validators.required)
    });
    this.contactService.getContactPage().subscribe((response: any) => {
      response.data.lat = Number(response.data.lat);
      response.data.lon = Number(response.data.lon);
      response.data.first_office = this.isJSON(response.data.first_office);
      response.data.question = this.isJSON(response.data.question);
      response.data.job = this.isJSON(response.data.job);
      this.contactData = response.data;
      this.loadingService.hideLoading();
    });
  }

  sendMessage() {
    if (this.sendMailForm.invalid) {
      swal(
        'Alerta',
        'Por favor llene todos los campos y verifique que estén correctamente llenos para poder enviar el mensaje', 'warning');
    } else {
      this.contactService.sendMessage(this.sendMailForm.value).subscribe((response: any) => {
        swal('Exito', 'Su mensaje ha sido enviado con éxito', 'success');
      }, (error: any) => {
        swal('Error', 'Lo sentimos se ha producido un error interno, por favor inténtelo nuevamente en unos minutos', 'error');
      });
    }

  }

}
