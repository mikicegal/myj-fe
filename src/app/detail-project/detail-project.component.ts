import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ProjectsService } from '../admin-services/projects.service';
import { Subscription } from 'rxjs';
import { LoadingService } from '../shared/loading/loading.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detail-project',
  templateUrl: './detail-project.component.html',
  styleUrls: ['./detail-project.component.scss']
})
export class DetailProjectComponent implements OnInit, OnDestroy {
  detailConfig;
  projectData;
  images;
  projectId;
  bim;
  activeBim;
  idxActualBim;
  activeBimConfig;
  isSpecial: boolean;
  routerSubscription: Subscription;
  extractedURL;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private projectService: ProjectsService,
              public loadingService: LoadingService,
              public domSanitizer: DomSanitizer
  ) {

  }
  isJSON(json) {
    try {
      return JSON.parse(json);
    } catch (e) {
      return json;
    }
  }
  ngOnInit() {
    this.loadingService.showLoading();
    this.projectId = this.route.snapshot.params.id;
    this.routerSubscription = this.router.events.subscribe((change) => {
      if (change instanceof NavigationEnd) {
        this.retrieveData();
      }
    });
    this.retrieveData();
  }
  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }
  retrieveData() {
    this.projectService.getDetailProject(this.projectId).subscribe((response: any) => {
      this.images = response.data.projects;
      this.isSpecial = response.data.have_bim;
      if (this.isSpecial) {
        this.bim = response.data.bim;
        this.setBimActive(0);
      }
      const arrayImages = [];
      this.images.forEach(element => {
        arrayImages.push({ image: element.path_image });
      });
      this.detailConfig = {
        arraySliders: arrayImages,
        numberSliderPerView: 1,
        transitionSpeed: 8000,
        arrowTransparent: true,
        thumbnails: false
      };
      this.projectData = response.data.project;
      if (this.projectData.url_video) {
        this.extractedURL = this.domSanitizer.bypassSecurityTrustResourceUrl(
          'https://www.youtube.com/embed/' +
          this.extractYoutubeVideoURL(this.projectData.url_video));
      }


      this.projectData.descripcion = this.isJSON(response.data.project.descripcion);
      this.loadingService.hideLoading();
    });
  }
  setBimActive(index) {
    this.activeBim = null;
    setTimeout(() => {
      this.idxActualBim = index;
      this.activeBim = this.bim[index];
      this.activeBim.descripcion = this.isJSON(this.activeBim.descripcion);
      const arrayImages = [];
      this.activeBim.imagenes.forEach(element => {
        arrayImages.push({ image: element.url_image });
      });
      this.activeBimConfig = {
        arraySliders: arrayImages,
        numberSliderPerView: 1,
        transitionSpeed: 8000,
        arrowTransparent: true,
        thumbnails: false
      };
    }, 200);
  }
  extractYoutubeVideoURL(url) {
    let videoId = url.split('v=')[1];
    const ampersandPosition = videoId.indexOf('&');
    if (ampersandPosition !== -1) {
      return videoId = videoId.substring(0, ampersandPosition);
    }
    return videoId;
  }
}
