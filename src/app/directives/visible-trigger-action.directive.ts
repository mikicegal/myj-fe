import { Directive, ElementRef, Output, EventEmitter, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appVisibleTriggerAction]'
})
export class VisibleTriggerActionDirective implements AfterViewInit {
  @Output() appVisibleTriggerAction: EventEmitter<any> = new EventEmitter();
  constructor(private element: ElementRef) { }
  private intersectionObserver: IntersectionObserver;
  ngAfterViewInit() {
    this.intersectionObserver = new IntersectionObserver(entries => {
      this.checkForIntersection(entries);
    }, {});
    this.intersectionObserver.observe(<Element>(this.element.nativeElement));
  }
  private checkForIntersection = (entries: Array<IntersectionObserverEntry>) => {
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (this.checkIfIntersecting(entry)) {
        this.appVisibleTriggerAction.emit();
        this.intersectionObserver.unobserve(<Element>(this.element.nativeElement));
        this.intersectionObserver.disconnect();
      }
    });
  }
  private checkIfIntersecting(entry: IntersectionObserverEntry) {
    return (<any>entry).isIntersecting && entry.target === this.element.nativeElement;
  }
}
