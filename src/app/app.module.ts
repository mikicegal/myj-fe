import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import { OurCompanyComponent } from './our-company/our-company.component';
import { ProjectsComponent } from './projects/projects.component';
import { ContactComponent } from './contact/contact.component';
import { AgmCoreModule } from '@agm/core';
import { RouterModule } from '@angular/router';
import { SigPoliticsComponent } from './sig-politics/sig-politics.component';
import { CertificationsComponent } from './certifications/certifications.component';
import { DetailProjectComponent } from './detail-project/detail-project.component';
import { ServicesComponent } from './services/services.component';
import { DetailServiceComponent } from './detail-service/detail-service.component';
import { VisibleTriggerActionDirective } from './directives/visible-trigger-action.directive';
import { OurHomeComponent } from './our-home/our-home.component';
import { CategoryProjectComponent } from './category-project/category-project.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {NgsRevealModule} from 'ngx-scrollreveal';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OurCompanyComponent,
    ProjectsComponent,
    ContactComponent,
    SigPoliticsComponent,
    CertificationsComponent,
    DetailProjectComponent,
    ServicesComponent,
    DetailServiceComponent,
    VisibleTriggerActionDirective,
    OurHomeComponent,
    CategoryProjectComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCS-uXkMAWb1aC25eTMiXf-erYvprK8XGY'
    }),
    NgsRevealModule,
    FormsModule,
    PdfViewerModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
