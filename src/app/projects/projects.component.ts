import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../admin-services/projects.service';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor(private projectService: ProjectsService, public loadingService: LoadingService) { }
  categoryProjectsData;
  banner;
  ngOnInit() {
    this.loadingService.showLoading();
    this.projectService.getCategoryProjects().subscribe((response: any) => {
      this.categoryProjectsData = response.data.categories;
      this.banner = response.data.banner;
      this.loadingService.hideLoading();
    });
  }

}
