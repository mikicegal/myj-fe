import { Component, OnInit } from '@angular/core';
import { UsService } from '../admin-services/us.service';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-our-home',
  templateUrl: './our-home.component.html',
  styleUrls: ['./our-home.component.scss']
})
export class OurHomeComponent implements OnInit {

  constructor(private usService: UsService, public loadingService:LoadingService) { }
  ourHomeData;

  isJSON(json) {
    try {
      return JSON.parse(json)
    } catch (e) {
      return json
    }
  }

  ngOnInit() {
    this.loadingService.showLoading();
    this.usService.getOurHomePage().subscribe((response: any) => {
      this.ourHomeData = response.data.us;
      this.ourHomeData.quienes_somos = this.isJSON(this.ourHomeData.quienes_somos);
      this.ourHomeData.politicas_sig = this.isJSON(this.ourHomeData.politicas_sig);
      this.ourHomeData.certificaciones = this.isJSON(this.ourHomeData.certificaciones);
      this.loadingService.hideLoading();
    });
  }

}
