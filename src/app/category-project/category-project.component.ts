import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectsService } from '../admin-services/projects.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-category-project',
  templateUrl: './category-project.component.html',
  styleUrls: ['./category-project.component.scss']
})
export class CategoryProjectComponent implements OnInit, OnDestroy {
  routerSubscription: Subscription;
  projects;
  banner;
  categoryId;
  constructor(private projectService: ProjectsService,
    private route: ActivatedRoute,
    private router: Router,
    public loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.showLoading();
    this.categoryId = this.route.snapshot.params['category'];
    this.routerSubscription = this.router.events.subscribe((change) => {
      if (change instanceof NavigationEnd) {
        this.categoryId = this.route.snapshot.params['category'];
        this.retrieveData();
      }
    });
    this.retrieveData();
    
  }
  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }
  retrieveData() {
    this.projectService.getListProjectsByCategory(this.categoryId).subscribe((response: any) => {
      this.banner = response.data.category;
      this.projects = response.data.projects;
      this.loadingService.hideLoading();
    })
  }

}
