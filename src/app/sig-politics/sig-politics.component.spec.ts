import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigPoliticsComponent } from './sig-politics.component';

describe('SigPoliticsComponent', () => {
  let component: SigPoliticsComponent;
  let fixture: ComponentFixture<SigPoliticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigPoliticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigPoliticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
