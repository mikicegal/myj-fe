import { Component, OnInit } from '@angular/core';
import { UsService } from '../admin-services/us.service';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-sig-politics',
  templateUrl: './sig-politics.component.html',
  styleUrls: ['./sig-politics.component.scss']
})
export class SigPoliticsComponent implements OnInit {

  constructor(private usService: UsService, public loadingService: LoadingService) { }
  sigData;
  ngOnInit() {
    this.loadingService.showLoading();
    this.usService.getSIGPoliticsPage().subscribe((response: any) => {
      this.sigData = response.data.sig;
      this.loadingService.hideLoading();
    });
  }

}
