import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { OurCompanyComponent } from './our-company/our-company.component';
import { ProjectsComponent } from './projects/projects.component';
import { SigPoliticsComponent } from './sig-politics/sig-politics.component';
import { CertificationsComponent } from './certifications/certifications.component';
import { DetailProjectComponent } from './detail-project/detail-project.component';
import { ServicesComponent } from './services/services.component';
import { DetailServiceComponent } from './detail-service/detail-service.component';
import { OurHomeComponent } from './our-home/our-home.component';
import { CategoryProjectComponent } from './category-project/category-project.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'our-company', component: OurCompanyComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'sig-politics', component: SigPoliticsComponent },
  { path: 'certifications', component: CertificationsComponent },
  { path: 'projects/:category', component: CategoryProjectComponent },
  { path: 'projects/:category/:id', component: DetailProjectComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'services/:id', component: DetailServiceComponent },
  { path: 'our-home', component: OurHomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
