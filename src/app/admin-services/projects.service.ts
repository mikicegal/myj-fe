import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private http: HttpClient) { }
  getCategoryProjects() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'projects', { headers });
  }
  getListProjectsByCategory(idCategory) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'category_project/' + idCategory, { headers });
  }
  getDetailProject(idProject) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'project_detail/' + idProject, { headers });
  }
}
