import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }
  getHomePage() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'home');
  }
}
