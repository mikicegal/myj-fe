import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  constructor(private http:HttpClient) { }
  getFooterData(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'footer', { headers });
  }
}
