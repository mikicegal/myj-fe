import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsService {

  constructor(private http: HttpClient) { }
  gerOurCompanyPage(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'howus', { headers });
  }
  getOurHomePage(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'us', { headers });
  }
  getSIGPoliticsPage(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'sig', { headers });
  }
  getCertificationsPage(){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'certifications', { headers });
  }
}
