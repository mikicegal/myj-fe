import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }
  getListServices() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'services', { headers });
  }
  getDetailService(id) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(environment.backend_url + 'services/getService/' + id, { headers });
  }
}
