import { Component, OnInit, Sanitizer } from '@angular/core';
import { UsService } from '../admin-services/us.service';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingService } from '../shared/loading/loading.service';

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.scss']
})
export class CertificationsComponent implements OnInit {

  constructor(private usService: UsService, private sanitizer: DomSanitizer, public loadingService: LoadingService) { }
  certificationData;
  arrayCertifications;
  arrayHomologations;

  isJSON(json) {
    try {
      return JSON.parse(json)
    } catch (e) {
      return json
    }
  }

  ngOnInit() {
    this.loadingService.showLoading();
    this.usService.getCertificationsPage().subscribe((response: any) => {
      this.certificationData = response.data.certification;
      this.certificationData.descripcion = this.sanitizer.bypassSecurityTrustHtml(this.isJSON(this.certificationData.descripcion));
      this.certificationData.descripcion_homologacion = this.sanitizer.bypassSecurityTrustHtml(this.isJSON(this.certificationData.descripcion_homologacion));
      this.arrayCertifications = response.data.certifications;
      this.arrayHomologations = response.data.homologaciones;
      this.loadingService.hideLoading();
    });
  }

}
