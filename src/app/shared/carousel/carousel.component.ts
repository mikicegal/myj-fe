import { Component, OnInit, AfterViewInit, ElementRef, Input, ViewChild } from '@angular/core';
export interface ICarouselConfig {
  transitionSpeed: number;
  numberSliderPerView: number;
  arraySliders: Array<any>;
  style?: any;
  contain?: boolean;
}


@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, AfterViewInit {

  @ViewChild('outerContainer', { static: true }) outerContainer: ElementRef;
  @ViewChild('innerContainer', { static: true }) innerContainer: ElementRef;
  @Input() config: ICarouselConfig;
  private showList: NodeList;
  private sizePerSlide: number;
  constructor() {

  }

  ngOnInit() {
    this.sizePerSlide = 100 / this.config.numberSliderPerView;
    if (this.config.arraySliders.length > this.config.numberSliderPerView) {
      setInterval(() => {
        this.showList = this.innerContainer.nativeElement.querySelectorAll('li');
        this.innerContainer.nativeElement.style.transition = 'all .5s';
        this.innerContainer.nativeElement.style.transform = 'translateX(-' + this.sizePerSlide + '%)';
      }, this.config.transitionSpeed);
    }
    this.listenerTransition();

  }
  ngAfterViewInit() {
    this.showList = this.innerContainer.nativeElement.querySelectorAll('li');
    this.showList.forEach((element: HTMLElement) => {
      element.style.width = this.sizePerSlide + '%';
      element.style.height = '100%';
    });
  }

  listenerTransition() {
    this.innerContainer.nativeElement.addEventListener('transitionend', event => {
      const list = this.innerContainer.nativeElement.querySelectorAll('li');
      this.innerContainer.nativeElement.style.transition = 'none';
      this.innerContainer.nativeElement.style.transform = 'translateX(0)';
      this.innerContainer.nativeElement.appendChild(list[0].cloneNode(true));
      this.innerContainer.nativeElement.removeChild(list[0]);
    }, false);
  }


}
