import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loadingsubject = new BehaviorSubject({ loaded: false });
  loadingObservable = this.loadingsubject.asObservable();
  constructor() { }
  showLoading() {
    this.loadingsubject.next({ loaded: false });
  }
  hideLoading() {
    this.loadingsubject.next({ loaded: true });
  }


}
