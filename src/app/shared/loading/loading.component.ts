import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from './loading.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {
  loaded;
  listener: Subscription;
  constructor(loadingService: LoadingService) {
    this.listener = loadingService.loadingObservable.subscribe((state) => {
      this.loaded = state.loaded;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.listener.unsubscribe();
  }
}
