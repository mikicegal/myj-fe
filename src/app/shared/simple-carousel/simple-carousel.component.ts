import { Component, OnInit, Input, ViewChildren, ElementRef, QueryList, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-simple-carousel',
  templateUrl: './simple-carousel.component.html',
  styleUrls: ['./simple-carousel.component.scss']
})
export class SimpleCarouselComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() config;
  @ViewChildren('listSlides') listSlides: QueryList<ElementRef>;
  @ViewChildren('selectors') selectorsButton: QueryList<ElementRef>;
  @ViewChildren('thumbnails') thumbnails: QueryList<ElementRef>;
  nextSlide = 0;
  active = 1;
  semaphore = 1;
  interval: any;
  constructor() { }

  ngOnInit() {

  }
  ngAfterViewInit() {
    this.listSlides.toArray()[0].nativeElement.classList.add('active');
    if (!this.config.thumbnails) {
      this.selectorsButton.toArray()[0].nativeElement.classList.add('active');
    }
    if (this.config.thumbnails) {
      this.thumbnails.toArray()[0].nativeElement.classList.add('active');
    }
    this.interval = setInterval(() => {
      this.next();
    }, this.config.transitionSpeed || 5000)
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }
  next() {
    if (this.listSlides.length > 1 && this.semaphore) {
      this.semaphore = 0;
      if ((this.listSlides.length - 1) == this.nextSlide) {
        this.nextSlide = 0;
        this.setActive();
      }
      else {
        this.nextSlide++;
        this.setActive();
      }



      this.semaphore = 1;
    }
  }
  prev() {
    if (this.listSlides.length > 1 && this.semaphore) {
      this.semaphore = 0;
      if (this.nextSlide == 0) {
        this.nextSlide = this.listSlides.length - 1;
        this.setActive();
      } else {
        this.nextSlide--;
        this.setActive();
      }
      this.semaphore = 1;
    }
  }
  setActive(idx?) {
    this.listSlides.toArray().forEach((element) => {
      element.nativeElement.classList.remove('active');
    })
    if (!this.config.thumbnails) {
      this.selectorsButton.toArray().forEach((element) => {
        element.nativeElement.classList.remove('active');
      });
    }
    if (this.config.thumbnails) {
      this.thumbnails.toArray().forEach((element) => {
        element.nativeElement.classList.remove('active');
      });
    }
    this.listSlides.toArray()[idx || this.nextSlide].nativeElement.classList.add('active');
    if (!this.config.thumbnails) {
      this.selectorsButton.toArray()[idx || this.nextSlide].nativeElement.classList.add('active');
    }
    if (this.config.thumbnails) {
      this.thumbnails.toArray()[idx || this.nextSlide].nativeElement.classList.add('active');
    }
  }
  goSlide(idx) {
    if (this.semaphore) {
      this.semaphore = 0;
      this.setActive(idx);
      this.nextSlide = idx;
      this.semaphore = 1;
    }
  }
}
