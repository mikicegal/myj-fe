import { Component, OnInit } from '@angular/core';
import { FooterService } from 'src/app/admin-services/footer.service';
import { zip } from 'rxjs';
import { ServiceService } from 'src/app/admin-services/service.service';
import { ProjectsService } from 'src/app/admin-services/projects.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerData;
  servicesData;
  projectsData;
  collapsables = new Array(4).fill(false);
  certifications;
  certificationsConfig;
  constructor(private serviceService: ServiceService, private projectsService: ProjectsService, private footerService: FooterService) { }

  ngOnInit() {
    zip(this.serviceService.getListServices(), this.projectsService.getCategoryProjects()).subscribe((response: any) => {
      this.servicesData = response[0].data;
      this.projectsData = response[1].data;
    });
    this.footerService.getFooterData().subscribe((response: any) => {
      this.footerData = response.data.footer;
      this.certifications = response.data.certifications;
      const certificationArray = new Array();
      this.certifications.forEach(element => {
        certificationArray.push(element.path_document);
      });
      this.certificationsConfig = {
        arraySliders: certificationArray,
        numberSliderPerView: 6,
        transitionSpeed: 6000,
        contain: true
      };
    });
  }
  toggleCollapsable(index) {
    this.collapsables[index] = !this.collapsables[index];
  }

}
