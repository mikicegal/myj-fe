import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/admin-services/service.service';
import { zip } from 'rxjs';
import { ProjectsService } from 'src/app/admin-services/projects.service';
import { FooterService } from 'src/app/admin-services/footer.service';
import { HomeService } from 'src/app/admin-services/home.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private serviceService: ServiceService,
    private projectsService: ProjectsService,
    private footerService: FooterService,
    private homeService: HomeService) { }
  footerData;
  servicesData;
  projectsData;
  logoData;
  showMobile = false;
  ngOnInit() {
    zip(
      this.serviceService.getListServices(),
      this.projectsService.getCategoryProjects(),
      this.homeService.getHomePage()).subscribe((response: any) => {
        this.servicesData = response[0].data;
        this.projectsData = response[1].data;
        console.log('test', response[2].data.home.url_logo);
        this.logoData = response[2].data.home.url_logo;
      });
    this.footerService.getFooterData().subscribe((response: any) => {
      this.footerData = response.data.footer;
    });
  }
  showMobileOptions() {
    this.showMobile = !this.showMobile;
  }

}
