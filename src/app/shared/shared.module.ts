import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CarouselComponent } from './carousel/carousel.component';
import { RouterModule } from '@angular/router';
import { SimpleCarouselComponent } from './simple-carousel/simple-carousel.component';
import { LoadingComponent } from './loading/loading.component';
import { NgsRevealModule } from 'ngx-scrollreveal';



@NgModule({
  declarations: [HeaderComponent, FooterComponent, CarouselComponent, SimpleCarouselComponent, LoadingComponent],
  imports: [
    RouterModule,
    CommonModule,
    NgsRevealModule,
  ],
  exports: [HeaderComponent, FooterComponent, CarouselComponent, SimpleCarouselComponent, LoadingComponent]
})
export class SharedModule { }
