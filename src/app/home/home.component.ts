import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { ICarouselConfig } from '../shared/carousel/carousel.component';
import { HomeService } from '../admin-services/home.service';
import { zip } from 'rxjs';
import { ServiceService } from '../admin-services/service.service';
import { LoadingService } from '../shared/loading/loading.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChildren('numbers') numbers: QueryList<ElementRef>;
  homeData;
  servicesData;
  arraySliders;
  hasPopup;
  popup;
  mainConfig: any;
  clientSlider1: ICarouselConfig;
  clientSlider2: ICarouselConfig;

  constructor(
    private homeService: HomeService,
    private serviceService: ServiceService,
    public loadingService: LoadingService,
    private domSanitizer: DomSanitizer) {
    loadingService.showLoading();
    zip(this.homeService.getHomePage(), this.serviceService.getListServices()).subscribe((response: any) => {
      this.setupHomeData(response[0]);
      this.servicesData = response[1].data.services;
      const arrayclients: Array<any> = response[0].data.clients;
      this.hasPopup = response[0].data.hasPopup;
      this.popup = response[0].data.popup[0];
      this.popup.titulo = domSanitizer.bypassSecurityTrustHtml(this.isJSON(this.popup.titulo));
      this.popup.descripcion = domSanitizer.bypassSecurityTrustHtml(this.isJSON(this.popup.descripcion));

      const sub1 = [];
      const sub2 = [];
      arrayclients.forEach((element, index) => {
        if (index < 15) {
          sub1.push(element.path_image);
        } else {
          sub2.push(element.path_image);
        }
      });
      if (document.body.clientWidth >= 425) {
        this.clientSlider1 = {
          arraySliders: sub1,
          numberSliderPerView: 6,
          transitionSpeed: 3000,
          contain: true
        };
        if (sub2.length > 0) {
          this.clientSlider2 = {
            arraySliders: sub2,
            numberSliderPerView: 6,
            transitionSpeed: 3000,
            contain: true
          };
        }
      } else {
        this.clientSlider1 = {
          arraySliders: sub1,
          numberSliderPerView: 3,
          transitionSpeed: 3000,
          contain: true
        };
        if (sub2.length > 0) {
          this.clientSlider2 = {
            arraySliders: sub2,
            numberSliderPerView: 3,
            transitionSpeed: 3000,
            contain: true
          };
        }
      }

      loadingService.hideLoading();
    });
  }
  isJSON(json) {
    try {
      return JSON.parse(json);
    } catch (e) {
      return json;
    }
  }

  private setupHomeData(response: any) {

    response.data.home.anios_exp = Number(response.data.home.anios_exp);
    response.data.home.clientes = Number(response.data.home.clientes);
    response.data.home.proyectos = Number(response.data.home.proyectos);
    response.data.home.certificaciones = Number(response.data.home.certificaciones);
    this.homeData = response.data;
    if (this.homeData.sliders) {
      this.arraySliders = [];
      this.homeData.sliders.forEach((item) => {
        this.arraySliders.push({
          image: item.path_image,
          title: item.titulo_banner,
          subtitle: item.frase_banner,
          url_page: item.url_page
        });
      });
      this.mainConfig = {
        arraySliders: this.arraySliders,
        numberSliderPerView: 1,
        transitionSpeed: 8000
      };
    }
  }

  ngOnInit() {
  }
  showAnimateNumbers() {
    for (const item of this.numbers.toArray()) {
      let initialValue = 0;
      const timer = item.nativeElement.dataset.number;
      const interval = setInterval(() => {
        if (initialValue < item.nativeElement.dataset.number) {
          item.nativeElement.innerHTML =
            item.nativeElement.dataset.prefix ? item.nativeElement.dataset.prefix + initialValue : initialValue;
        } else {
          clearInterval(interval);
          item.nativeElement.innerHTML =
            item.nativeElement.dataset.prefix ? item.nativeElement.dataset.prefix + initialValue : initialValue;
        }
        initialValue++;
      }, 1500 / timer);
    }
  }
  toggleIntroModal() {
    this.hasPopup = !this.hasPopup;
  }

}
